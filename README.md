Baseado no artigo https://sourcemaking.com/refactoring/replace-type-code-with-subclasses minha
primeira intenção foi substituir o `type` de `product` por classes reais.

Em uma segunda análise, vi que o ideal seria colocar toda a lógica de processamento de produtos
em um tipo de classe separada que seria responsável por essa parte de comportamento
diferente segundo cada tipo de produto. A ideia veio do artigo em
https://brizeno.wordpress.com/2011/08/31/strategy/, para usar o padrão strategy e assim,
implementar os comportamentos dinamicamente.
