class Payment
  attr_reader :authorization_number, :amount, :invoice, :order, :payment_method, :paid_at

  def initialize(attributes = {})
    @authorization_number, @amount = attributes.values_at(:authorization_number, :amount)
    @invoice, @order = attributes.values_at(:invoice, :order)
    @payment_method = attributes.values_at(:payment_method)
  end

  def pay(paid_at = Time.now)
    @amount = order.total_amount
    @authorization_number = Time.now.to_i
    @invoice = Invoice.new(billing_address: order.address, shipping_address: order.address, order: order)
    @paid_at = paid_at
    order.close(@paid_at)
  end

  def paid?
    !paid_at.nil?
  end
end

class Invoice
  attr_reader :billing_address, :shipping_address, :order

  def initialize(attributes = {})
    @billing_address = attributes.values_at(:billing_address)
    @shipping_address = attributes.values_at(:shipping_address)
    @order = attributes.values_at(:order)
  end
end

class Order
  attr_reader :customer, :items, :payment, :address, :closed_at

  def initialize(customer, overrides = {})
    @customer = customer
    @items = []
    @order_item_class = overrides.fetch(:item_class) { OrderItem }
    @address = overrides.fetch(:address) { Address.new(zipcode: '45678-979') }
  end

  def add_product(product)
    @items << @order_item_class.new(order: self, product: product)
  end

  def total_amount
    @items.map(&:total).inject(:+)
  end

  def close(closed_at = Time.now)
    @closed_at = closed_at
  end
end

class OrderItem
  attr_reader :order, :product

  def initialize(order:, product:)
    @order = order
    @product = product
  end

  def total
    10
  end

  def delivery_strategy
    @product.delivery_strategy
  end
end

class OrderProcessor
  attr_reader :order
  def initialize(order)
    @order = order
  end

  def call
    @order.items.each do |item|
      process(item)
    end
  end

  private

  def process(item)
    item.delivery_strategy.new(item).call
  end
end

class Address
  attr_reader :zipcode

  def initialize(zipcode:)
    @zipcode = zipcode
  end
end

class CreditCard
  def self.fetch_by_hashed(code)
    CreditCard.new
  end
end

class Customer
  attr_accessor :email

  def initialize(email:)
     @email = email
  end
end

class Product
  attr_reader :name, :delivery_strategy

  def initialize(name:, delivery_strategy:)
    @name = name
    @delivery_strategy = delivery_strategy
  end
end

class Membership < Product
  def initialize(name:, delivery_strategy: DeliveryStrategy::Membership)
    super
  end
end

class Physical < Product
  def initialize(name:, delivery_strategy: DeliveryStrategy::Physical)
    super
  end
end

class Book < Product
  def initialize(name:, delivery_strategy: DeliveryStrategy::Book)
    super
  end
end

class Digital < Product
  def initialize(name:, delivery_strategy: DeliveryStrategy::Digital)
    super
  end
end

# ----------------Estratégias-----------------
module DeliveryStrategy
  attr_reader :item
  class Strategy
    def initialize(item)
      @item = item
    end
  end

  class Book < Strategy
    def call
      generate_label
    end

    private
    def generate_label
      p "A compra do #{@item.product.name} é um item isento de impostos conforme disposto na Constituição Art. 150, VI, d."
    end
  end

  class Membership < Strategy
    def call
      activate_membership
      send_email
    end

    private

    def send_email
      p "Email sobre compra de #{@item.product.name}"
    end

    def activate_membership
      p "Assinatura ativada"
    end
  end

  class Physical < Strategy
    def call
      generate_label
    end

    private

    def generate_label
      p "Default label da compra de #{@item.product.name}"
    end
  end

  class Digital < Strategy
    def call
      send_email
      generate_voucher
    end

    private

    def send_email
      p "Email sobre compra de #{@item.product.name}"
    end

    def generate_voucher
      p "Voucher no valor de 10 reais recebido pela compra de #{@item.product.name}"
    end
  end
end

foolano = Customer.new(email: 'foolano@net.br')
order = Order.new(foolano)
physical = Physical.new(name: "Colete à prova de dívidas")
membership = Membership.new(name: "Assinatura vitalícia de brindes de montão")
book = Book.new(name: "Vida e obra de um endividado")
digital = Digital.new(name: "Videos sobre como pagar o cartao de credito")
order.add_product(book)
order.add_product(digital)
order.add_product(physical)
order.add_product(membership)

payment = Payment.new(order: order, payment_method: CreditCard.fetch_by_hashed('43567890-987654367'))
payment.pay

OrderProcessor.new(order).call if payment.paid?
